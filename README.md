# e3-iocmetadata

Wrapper for the EPICS module [iocMetadata](https://gitlab.esss.lu.se/icshwi/iocmetadata.git).

## Summary

This module scans the record names of its IOC, searching for specific info tags. It then creates a NTTable PV that contains the name of these records in its value. It is a way to expose the info tags of an IOC using PVAccess. A detailed explanation and examples are in the [README](https://gitlab.esss.lu.se/icshwi/iocmetadata/-/blob/master/README.md) file of the main EPICS module repository.

## Utilities

One can also use the `pvlistget.py` script to obtain a plain list of the values (depends on [p4p](https://mdavidsaver.github.io/p4p/)).
Considering `pvlistFromInfo` usage example in [iocMetadata](https://gitlab.esss.lu.se/icshwi/iocmetadata.git) README:

```
$> python3 pvlistget.py EXAMPLE:ArchiverList
EXAMPLE-A:SignalA.VAL
EXAMPLE-A:SignalA.EGU
EXAMPLE-B:SignalA.VAL
EXAMPLE-B:SignalA.EGU
EXAMPLE-C:SignalA.VAL
EXAMPLE-C:SignalA.EGU
EXAMPLE-D:SignalA.VAL
EXAMPLE-D:SignalA.EGU

$> python3 pvlistget.py EXAMPLE:SaveRestoreList
EXAMPLE-A:SignalB
EXAMPLE-B:SignalB
EXAMPLE-C:SignalB
EXAMPLE-D:SignalB