#!/usr/bin/python3

import queue
import sys

# Import p4p client context
from p4p.client.thread import Context


def main():

    # Check for at least one PV as input argument
    if (len(sys.argv)) < 2:
        print("[ERROR] No PV name specified.")
        return

    # Create PVA context
    ctxt = Context("pva", nt=False)

    try:
        # Take only the first PV name and try to get from the network
        pvlist_ = ctxt.get(str(sys.argv[1]), timeout=3, throw=True)

        if pvlist_.has("pvlist"):
            # Print a simple list of the content
            for i in pvlist_.pvlist:
                print(i)
        else:
            print("[ERROR] PV", sys.argv[1], "does not have pvlist field")
    except (queue.Empty, TimeoutError):
        print("[ERROR] Unable to connect to PV", sys.argv[1])

    ctxt.close()
    return


if __name__ == "__main__":
    main()
