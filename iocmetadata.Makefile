#
#  Copyright (c) 2019 - 2022, European Spallation Source ERIC
#
#  The program is free software: you can redistribute it and/or modify it
#  under the terms of the BSD 3-Clause license.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.
#
# Author  : JoaoPaulo Martins
# email   : joaopaulo.martins@ess.eu
# Date    : 2021-08-04
# version : 0.0.0
#

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


APP:=iocMetadataApp
APPSRC:=$(APP)/src

SOURCES   += $(APPSRC)/infoTableRec.cpp
SOURCES   += $(APPSRC)/infoTagCollector.cpp

SOURCES   += $(APPSRC)/pvListRec.cpp
SOURCES   += $(APPSRC)/pvListCollector.cpp

DBDS   += $(APPSRC)/infoTagCollector.dbd
DBDS   += $(APPSRC)/pvListCollector.dbd

SCRIPTS += $(wildcard iocsh/*.iocsh)

.PHONY: vlibs
vlibs:
