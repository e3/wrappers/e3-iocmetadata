require iocmetadata

dbLoadRecords("$(E3_CMD_TOP)/counters.template", "P=IOC1")
dbLoadRecords("$(E3_CMD_TOP)/infotags.template", "ITAG=TEST1, P=IOC1, N=0, M=0")

iocMetadataConfigure("TEST1", "IOC1")

iocInit()
